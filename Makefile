JFLAGS = -g
JC = javac

#Directories
JCLASSDIR = bin
JSOURCEDIR = src-finalfinal

PACKDIR = client
PACK = za.ac.sun.cs.chat

%.class: %.java
				$(JC) $(JFLAGS) $<

# Set sources
SRCS := $(wildcard $(JSOURCEDIR)/$(PACKDIR)/*.java)
SRCS += $(wildcard $(JSOURCEDIR)/$(PACKDIR)/anotherdir/*.java)

# Set classes
CLS := $(wildcard $(JCLASSDIR)/$(PACKDIR)/*.class)
CLS += $(wildcard $(JCLASSDIR)/$(PACKDIR)/anotherdir/*.class)

SERVER = $(PACK).Server
CLIENT = $(PACK).Client

# Make commands
default: all

all:
	mkdir -p $(JCLASSDIR)
	$(JC) -d ./$(JSOURCEDIR) $(JFLAGS) $(SRCS)

.PHONY: server

server:
	mkdir -p $(JCLASSDIR)
	$(JC) -d ./$(JCLASSDIR) $(JFLAGS) $(SRCS)
	$(JVM) -cp ./$(JCLASSDIR) $(SERVER)

.PHONY: client

client:
	mkdir -p $(JCLASSDIR)
	$(JC) -d ./$(JCLASSDIR) $(JFLAGS) $(SRCS)
	$(JVM) -cp ./$(JCLASSDIR) $(CLIENT)

.PHONY: clean

clean:
	rm -rf $(JCLASSDIR)
