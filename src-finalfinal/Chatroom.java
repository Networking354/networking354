//disconnecting --> client sends disconnect msg to server, server removes client
// form list of connected clients and sends aknowledgement to client
//client exits.

import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.scene.text.Text;
import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import javafx.scene.text.FontWeight;
import javafx.scene.control.TextField;
import javafx.event.EventHandler;
import javafx.event.ActionEvent;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.layout.BorderPane;
import javafx.scene.control.Button;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.TilePane;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.geometry.Insets;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextArea;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.ScrollPane.ScrollBarPolicy;
import javafx.application.Platform;
import javafx.collections.*;

import communication.Packet;
import communication.ControlConstants;

import java.io.*;
import java.net.*;
import java.util.*;
import java.util.concurrent.*;

public class Chatroom extends Application implements ControlConstants   {

  private static ObjectInputStream ois;
  private static ObjectOutputStream oos;
  private static String username = "";
  private static Socket socket;

  static private Label status;
  static private Label users;
  static private TextField usernameInput;
  static private Text cmnd;
  static private Text received;
  static private Button disconnectButton;
  static private Button sendButton;
  static private ArrayList<String> connectedUsers;
  static private ComboBox<String> recipient;

  private class serverListner implements Runnable {

      public void run() {
        Boolean running = true;
        while(running) {
          try {
            Packet recv = (Packet) ois.readObject();
            int cmd = recv.getControlMsg();

            switch(cmd) {
              case CONNECTED:
                System.out.println("Received connection message from server");
                handleConnection(recv.getConn(), recv.getContent());
                System.out.println("Handled connection message from server");
                break;

              case NEW_USER:
                System.out.println("Received newuser message from server");
                handleNewUser(recv.getUser());
                System.out.println("Handled newuser message from server");
                break;

              case MESSAGE:
                System.out.println("Received  message from server");
                handleReceivedMessage(recv.getContent(), recv.getUser());
                System.out.println("Handled message from server");
                break;

              case DISCONNECTED_USER:
                System.out.println("Received disconnected user message from server");
                handleDisconnectedUser(recv.getUser());
                System.out.println("Handled disconnected user message from server");
                break;

              case DISCONNECT:
                System.out.println("Received disconnect acknw message from server");
                running = false;
                System.out.println("Handled disconnect acknw message from server");
                break;
            }

          } catch (Exception e) {
            System.out.println("Could not read object from ObjectInputStream\n");
            e.printStackTrace();
            System.exit(-1);
          }
        }
        handleDisconnect();
      }

      private void handleConnection(Boolean conn, String list) {
        if (conn) {
          String[] userlist = list.split(";");
          connectedUsers = new ArrayList<String>();
          String s = "";
          for (int i = 0; i < userlist.length; i++) {
            s += userlist[i] + "\n";
            System.out.println(userlist[i]);
            connectedUsers.add(userlist[i]);
          }
          final String ul = s;

          Platform.runLater(
              () -> {
                setConnected(ul);
              }
            );

        } else {
          Platform.runLater(
              () -> {
                setConnUnsuccessful();
              }
            );
        }
      }

      private void handleNewUser(String username) {
        connectedUsers.add(username);
        String newList = String.join("\n",connectedUsers);
        Platform.runLater(
            () -> {
              addNewUser(username, newList);
            }
          );
      }

      private void handleReceivedMessage(String msg, String sender) {
        Platform.runLater(
            () -> {
              newReceivedMessage(msg + " - Sent by " + sender + "\n\n");
            }
          );
      }

      private void handleDisconnectedUser(String user) {
        connectedUsers.remove(user);
        String newList = String.join("\n",connectedUsers);
        System.out.println("Received that someone disconnected");
        Platform.runLater(
            () -> {
              removeUser(newList);
            }
          );
      }

      private void handleDisconnect() {
        Platform.runLater(
            () -> {
              disconnect();
            }
          );
      }
    }

  @Override
  public void start(Stage primaryStage) throws Exception {
    /********Title*************************/
    Text title = new Text();
    title.setText("The CS354 Chatroom");
    title.setFont(Font.font("arial", FontWeight.EXTRA_BOLD, FontPosture.REGULAR,
                           20));

    /******Connected users********************/
    Text usersTitle = new Text("Connected Users");
    usersTitle.setFont(Font.font("arial", FontWeight.BOLD, FontPosture.REGULAR,
                           15));
    users = new Label("");

    VBox usersPane = new VBox(usersTitle, users);
    usersPane.setStyle("-fx-background-color: #6495ED;");
    usersPane.setAlignment(Pos.CENTER);
    usersPane.setMargin(usersTitle, new Insets(10));
    usersPane.setMargin(users, new Insets(10));

    /***********disconnecting************/
    disconnectButton = new Button("Disconnect");
    disconnectButton.setVisible(false);

    sendButton = new Button("Send");
    sendButton.setDisable(true);

    /********username input************/
    cmnd = new Text("Enter username to connect");
    usernameInput = new TextField();
    usernameInput.setPrefWidth(200);
    status = new Label("Disconnected");
    status.setFont(Font.font("arial", FontWeight.THIN,
                                  FontPosture.ITALIC, 12));

    //Connect to chatroom with unique username
    EventHandler<ActionEvent> event = new EventHandler<ActionEvent>() {

      @Override
      public void handle(ActionEvent event) {
        //get username and send to server
        username = usernameInput.getText();
        Packet usernameCheck = new Packet(USERNAME, null, false, username);
        try {
          oos.writeObject(usernameCheck);
          System.out.println("Send username to server");

        } catch (IOException e) {
          System.out.println("Could not write to ObjectOutputStream.");
          e.printStackTrace();
        }
      }
    };
    usernameInput.setOnAction(event);

    //handle disconnect Button
    EventHandler<ActionEvent> eventDisconnect = new EventHandler<ActionEvent>() {

      @Override
      public void handle(ActionEvent event) {
        Packet disconnectMsg = new Packet(DISCONNECT, null, false, null);
        try {
          oos.writeObject(disconnectMsg);
          System.out.println("Disconnect message sent to server.");

        } catch (IOException e) {
          System.out.println("Could not write to ObjectOutputStream.");
          e.printStackTrace();
        }
        status.setText("Disconnecting");
        sendButton.setDisable(true);
      }
    };
    disconnectButton.setOnAction(eventDisconnect);

    Text statusText = new Text("Status: ");
    statusText.setFont(Font.font("arial", FontWeight.THIN,
                                  FontPosture.ITALIC, 12));

    StackPane inputButtonStack = new StackPane(disconnectButton, usernameInput);

    HBox hboxConnect = new HBox(statusText, status);
    VBox vboxConnect = new VBox(cmnd, inputButtonStack, hboxConnect, usersPane);
    vboxConnect.setSpacing(10);

    /***********message input*******************/

    Text messageTitle = new Text("Chat to other users");
    messageTitle.setFont(Font.font("arial", FontWeight.BOLD, FontPosture.REGULAR,
                           15));

    TextArea messageInput = new TextArea();
    messageInput.setPrefColumnCount(30);
    messageInput.setPrefRowCount(5);

    Text sendcmd = new Text("Send to");
    recipient = new ComboBox<String>();
    //recipient.getItems().addAll("todo:add users");
    recipient.setVisibleRowCount(4);

    Label check = new Label("Please enter a message.");
    check.setFont(Font.font("arial", FontWeight.THIN,FontPosture.ITALIC, 12));
    check.setVisible(false);

    HBox send = new HBox(sendcmd, recipient, sendButton, check);
    send.setMargin(sendcmd, new Insets(10));
    send.setMargin(check, new Insets(10));
    send.setSpacing(5);

    /**********messages received****************/
    Text receivedTitle = new Text("Messages Received");
    receivedTitle.setFont(Font.font("arial", FontWeight.BOLD, FontPosture.REGULAR,
                           15));

    received = new Text("No messages to show yet");
    ScrollPane scrollPane = new ScrollPane(received);
    scrollPane.setHbarPolicy(ScrollBarPolicy.NEVER);
    scrollPane.setVbarPolicy(ScrollBarPolicy.AS_NEEDED);

    //handle send button
    EventHandler<ActionEvent> eventSend = new EventHandler<ActionEvent>() {

      @Override
      public void handle(ActionEvent event) {
        String s = messageInput.getText().trim();
        messageInput.setText("");
        if (s.isEmpty()) {
          check.setVisible(true);
        } else {
          String r = received.getText();
          if (r.equals("No messages to show yet")) {
            r = s + " - Sent by you\n\n";
          } else {
            r += s + " - Sent by you\n\n";
          }
          received.setText(r);
          //send message.
          String to = recipient.getValue().trim();
          Packet msg = new Packet(MESSAGE, s, false, to);

          try {
            oos.writeObject(msg);

          } catch (IOException e) {
            System.out.println("Could not write to ObjectOutputStream.");
            e.printStackTrace();
          }

        }
      }
    };
    sendButton.setOnAction(eventSend);

    /********message area layout****************/
    VBox messageArea = new VBox(messageTitle, messageInput, send, receivedTitle,
                                scrollPane);
    messageArea.setMargin(send, new Insets(10));
    messageArea.setMargin(scrollPane, new Insets(10));
    messageArea.setMargin(messageTitle, new Insets(10));
    messageArea.setMargin(receivedTitle, new Insets(10));

    /************Assemble**********************/
    BorderPane layout = new BorderPane();
    layout.setTop(title);
    layout.setLeft(vboxConnect);
    layout.setRight(messageArea);

    layout.setMargin(messageArea, new Insets(25));
    layout.setMargin(title, new Insets(25));
    layout.setMargin(vboxConnect, new Insets(25));
    /*******************************************/

    Group root = new Group(layout);
    Scene scene = new Scene(root, 800, 600);
    scene.setFill(Color.WHITE);

    primaryStage.setTitle("The Chatroom");
    primaryStage.setScene(scene);

    /////////////////////////////////////////////

    (new Thread(new serverListner())).start();

    /////////////////////////////////////////////

    primaryStage.show();
  }

  public static void setConnected(String ul) {
    status.setText("Connected");
    usernameInput.setVisible(false);
    cmnd.setVisible(false);
    disconnectButton.setVisible(true);
    ul = username + "\n------------------------\n" + ul;
    users.setText(ul);

    if (connectedUsers.size() > 0) {
      recipient.getItems().addAll("All");
      recipient.getItems().addAll(
        FXCollections.observableArrayList(connectedUsers)
      );
      recipient.setValue("All");
      sendButton.setDisable(false);
    }
  }

  public static void setConnUnsuccessful() {
    status.setText("Username already in use");
  }

  public static void addNewUser(String newUser, String newList) {
    newList = username + "\n------------------------\n" + newList;
    users.setText(newList);
    recipient.getItems().addAll(newUser);
  }

  public static void removeUser(String newList) {
    newList = username + "\n------------------------\n" + newList;
    users.setText(newList);
    String[] ul = newList.split("\n");
    recipient.setItems(FXCollections.observableArrayList(ul));
  }

  public void newReceivedMessage(String msg) {
    String s = received.getText();
    if (s.equals("No messages to show yet")) {
      s = msg + "\n\n";
    } else {
      s += msg + "\n\n";
    }
    received.setText(s);
  }

  public void disconnect() {
    try {
      ois.close();
      oos.close();
      socket.close();
      Platform.exit();
      System.exit(0);

    } catch (IOException e) {
      System.out.println(e);
    }
  }

  public static void main(String[] args) {
    //setup connection with server
    String ip = args[0];
		int port = Integer.parseInt(args[1]);

    try {
			socket = new Socket(ip, port);
      System.out.println("Connected to server");

			// obtaining input and out streams
			oos = new ObjectOutputStream(socket.getOutputStream());
			ois = new ObjectInputStream(socket.getInputStream());

      System.out.println("Connected to server");

      //launch application
      launch(args);

		} catch (Exception e) {
      System.out.println(e);
			e.printStackTrace();
		}
  }

}
