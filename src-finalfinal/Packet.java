package communication;

public class Packet {

  public enum Control {
    CONNECTED,
    DISCONNECT,
    MESSAGE,
    USERNAME,
    USERSLIST,
    NEWUSER;
  }

  private Control ctrl;
  private String content;
  private Boolean conn;
  private String user;

  public Packet(Control ctrl, String content, Boolean conn, String user) {
    this.ctrl = ctrl;
    this.content = content;
    this.conn = conn;
    this.user = user;
  }

  public Control getControlMsg() {
    return this.ctrl;
  }

  public String getContent() {
    return this.content;
  }

  public Boolean getConn() {
    return this.conn;
  }
}
