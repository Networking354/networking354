//package communication;

import java.io.*;
import java.text.*;
import java.util.*;
import java.net.*;
import java.util.concurrent.*;

import communication.Packet;
import communication.ControlConstants;


public class ClientHandler implements Runnable, ControlConstants{

	final ObjectInputStream dis; //change to object streams
	final ObjectOutputStream dos; //change to object streams for packet object
	final Socket s;
	private String username = "";

	ConcurrentHashMap<String, ClientHandler> clients ;



	// Constructor
  // change to object streams
	public ClientHandler(Socket s, ObjectInputStream dis, ObjectOutputStream dos, ConcurrentHashMap<String, ClientHandler> clients){

		this.s = s;
		this.dis = dis;
		this.dos = dos;
		this.clients = clients;
	}

	@Override
	public void run(){

		String message;
		Boolean running = true;
		String receiver = "";
		while(running) {


			try {
				//recieving the Packet
				Packet recvPack =  (Packet) dis.readObject();
				int cmd =  recvPack.getControlMsg();


				System.out.println("line 39");

				switch (cmd) {

					case USERNAME:
						username = recvPack.getUser();
						message = concatString(clients);
						if(clients.containsKey(username)){
							 Packet text = new Packet(CONNECTED, message , false , username);
							 dos.writeObject(text);
							 	System.out.println("Username is not okay");

						} else {

							clients.put(username, this);
							Packet text = new Packet(CONNECTED, message , true , username);
							Packet text2 = new Packet(NEW_USER, message , true, username);
							dos.writeObject(text);

							for ( String key: clients.keySet() ) {

								if(!key.equals(username)){
									clients.get(key).dos.writeObject(text2);
									System.out.println("Sending New User to other clients");
								}

							}

							System.out.println("Username is okay");

						}
						break;

					case MESSAGE:
						message = recvPack.getContent();
						receiver = recvPack.getUser();
						Packet text3 = new Packet(MESSAGE,message , true ,username);

						if (!receiver.equals("All")) {
							if (clients.containsKey(username)) {

								ClientHandler clientVal = clients.get(receiver);
								clientVal.dos.writeObject(text3);
								System.out.println("Send Message");
							}

						}else {
							for (String key: clients.keySet()) {

								if(!key.equals(username)){
									clients.get(key).dos.writeObject(text3);
									System.out.println("Send Message to all");
								}
							}
						}
						break;



					case DISCONNECT:
						/*communicating to other threads that a user wants to DISCONNECT*/

						message = "";
						Packet text = new Packet(DISCONNECTED_USER, message, true, username);
						Packet text2 = new Packet(DISCONNECT, message, true, username);

						for ( String key: clients.keySet() ) {

							if(!key.equals(username)){
								clients.get(key).dos.writeObject(text);
								System.out.println("Sending User Disconnect");
							}

						}

						clients.remove(username);
						this.dos.writeObject(text2);
						this.dis.close();
			      this.s.close();
						running = false ;
						break;

				}

				} catch (Exception e) {
					e.printStackTrace();

				}

			}

		}

		public String concatString(Map<String, ClientHandler> clients){

			String actClients = "";

			for(String key : clients.keySet()){
			actClients += key + ";";

			}
			return actClients;

		}


	}
