//packe communication;
import java.io.*;
import java.text.*;
import java.util.*;
import java.net.*;
import java.util.concurrent.*;

import communication.Packet;
import communication.ControlConstants;



public class Server extends Thread implements ControlConstants{

  /*creating the hashMap*/
  static ConcurrentHashMap<String, ClientHandler> activeClients = new ConcurrentHashMap<String, ClientHandler>();

  public static void main(String[] args) throws IOException {
		// set up server socket
		ServerSocket ss = new ServerSocket(5827);
    String str = "checking";

		// running infinite loop for getting
		// client request
		while (true){
			Socket s = null;

			try {
				// socket object to receive incoming client requests
				s = ss.accept();
        System.out.println("CONNECTED");

        /* getting the input and output streams*/
        ObjectInputStream input = new ObjectInputStream(s.getInputStream());
        ObjectOutputStream output = new ObjectOutputStream(s.getOutputStream());
        Packet text = new Packet(CONNECTED, str , true , str);

    
				// create a new thread object
        ClientHandler client = new ClientHandler(s, input , output, activeClients);
        Thread t = new Thread(client);

        //adding the client to the map

				// Invoking the start() method on thread
        t.start();

			}

			catch (Exception e){
				s.close();
				e.printStackTrace();
			}
		}
	}

}
