
import java.io.*;
import java.net.*;
import java.util.*;
import java.util.concurrent.*;
import communication.Packet;
import communication.ControlConstants;

public class Client implements ControlConstants {
	ConcurrentHashMap<String, Socket> activeClients = new ConcurrentHashMap<String, Socket>();

	public static void main(String[] args) throws IOException {

		Client client = new Client();
		String ip = args[0];
		int port = Integer.parseInt(args[1]);
		client.run(ip, port);
	}

	public static void run(String ip, int port) {
		try {
			// getting localhost ip
	  	//	InetAddress ip = InetAddress.getByName("localhost");

			// establish the connection with server port 5056

			Socket s = new Socket(ip, port);
			Scanner scn = new Scanner(System.in);

			// obtaining input and out streams
			ObjectOutputStream output = new ObjectOutputStream(s.getOutputStream());
			ObjectInputStream input = new ObjectInputStream(s.getInputStream());


			// the following loop performs the exchange of
			// information between client and client handler
			Packet recvPacket = (Packet) input.readObject();
			int cmd = recvPacket.getControlMsg();
			if (cmd == CONNECTED) {
				System.out.println("The connected test worked.");
			}
			while (true)
			{
				System.out.println("Server : " + input.readUTF());
				String tosend = scn.nextLine();
				output.writeUTF(tosend);

				// If client sends exit,close this connection
				// and then break from the while loop
				if(tosend.equals("Exit"))
				{
					System.out.println("Closing this connection : " + s);
					s.close();
					System.out.println("Connection closed");
					break;
				}

				// printing date or time as requested by client
				String received = input.readUTF();
				System.out.println(received);
			}

			// closing resources
			scn.close();
			input.close();
			output.close();
		} catch(Exception e){
			e.printStackTrace();
		}
	}
}
