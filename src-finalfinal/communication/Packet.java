package communication;

import java.io.Serializable;

public class Packet implements Serializable {

  /*public enum Control {
    CONNECTED,
    DISCONNECT,
    MESSAGE,
    USERNAME,
    USERSLIST,
    NEWUSER;
  }*/

  //private Control ctrl;
  private int ctrl;
  private String content;
  private Boolean conn;
  private String user;

  public Packet(int ctrl, String content, Boolean conn, String user) {
    this.ctrl = ctrl;
    this.content = content;
    this.conn = conn;
    this.user = user;
  }

  public int getControlMsg() {
    return this.ctrl;
  }

  public String getContent() {
    return this.content;
  }

  public Boolean getConn() {
    return this.conn;
  }

  public String getUser() {
    return this.user;
  }
}
