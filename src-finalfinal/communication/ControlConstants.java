package communication;

public interface ControlConstants {
  static final int CONNECTED = 0;
  static final int DISCONNECT = 1;
  static final int MESSAGE = 2;
  static final int USERNAME = 3;
  static final int DISCONNECTED_USER = 4;
  static final int NEW_USER = 5;
}
