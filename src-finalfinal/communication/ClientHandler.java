package communication;

import java.io.*;
import java.text.*;
import java.util.*;
import java.net.*;
import java.util.concurrent.*;

import communication.Packet;
import communication.Packet.Control;


public class ClientHandler implements Runnable{

	final ObjectInputStream dis; //change to object streams
	final ObjectOutputStream dos; //change to object streams for packet object
	final Socket s;


	// Constructor
  // change to object streams
	public ClientHandler(Socket s, ObjectInputStream dis, ObjectOutputStream dos){

		this.s = s;
		this.dis = dis;
		this.dos = dos;
	}

	@Override
	public void run(){

		String message;

		while(true) {

			try {
				//recieving the Packet
				Packet recvPack =  (Packet) dis.readObject();
				Control cmd =  recvPack.getControlMsg();


				System.out.println("line 39");

				switch (cmd) {
					case CONNECTED:
						message = recvPack.getContent();



					case DISCONNECT:
						this.s.close();
						break;


				}

				} catch (Exception e) {
					e.printStackTrace();

				}
					break;
			}
			try{

				// closing resources
				this.dis.close();
				this.dos.close();

			}catch(IOException e){
				e.printStackTrace();
			}
		}


	}
