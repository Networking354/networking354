import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.scene.text.Text;
import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import javafx.scene.text.FontWeight;
import javafx.scene.control.TextField;
import javafx.event.EventHandler;
import javafx.event.ActionEvent;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.layout.BorderPane;
import javafx.scene.control.Button;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.TilePane;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.geometry.Insets;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextArea;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.ScrollPane.ScrollBarPolicy;

public class GuiAppl extends Application {

  @Override
  public void start(Stage primaryStage) throws Exception {
    /********Title*************************/
    Text title = new Text();
    title.setText("The CS354 Chatroom");
    title.setFont(Font.font("arial", FontWeight.EXTRA_BOLD, FontPosture.REGULAR,
                           20));

    /******Connected users********************/
    Text usersTitle = new Text("Connected Users");
    usersTitle.setFont(Font.font("arial", FontWeight.BOLD, FontPosture.REGULAR,
                           15));
    Label users = new Label("");

    VBox usersPane = new VBox(usersTitle, users);
    //usersPane.setOrientation(Orientation.VERTICAL);
    usersPane.setStyle("-fx-background-color: #6495ED;");
    usersPane.setAlignment(Pos.CENTER);
    usersPane.setMargin(usersTitle, new Insets(10));
    usersPane.setMargin(users, new Insets(10));

    /***********disconnecting************/
    Button disconnectButton = new Button("Disconnect");
    disconnectButton.setVisible(false);

    /********username input************/
    Text cmnd = new Text("Enter username to connect");
    TextField usernameInput = new TextField();
    usernameInput.setPrefWidth(200);
    Label status = new Label("Disconnected");
    status.setFont(Font.font("arial", FontWeight.THIN,
                                  FontPosture.ITALIC, 12));
    EventHandler<ActionEvent> event = new EventHandler<ActionEvent>() {
      public void handle(ActionEvent e) {
        status.setText("Connected");
        String s = users.getText();
        s = s.concat(usernameInput.getText() + "\n");
        users.setText(s);
        usernameInput.setVisible(false);
        cmnd.setVisible(false);
        disconnectButton.setVisible(true);
      }
    };
    usernameInput.setOnAction(event);

    //handle disconnect Button
    event = new EventHandler<ActionEvent>() {
      @Override
      public void handle(ActionEvent e) {
        disconnectButton.setVisible(false);
        usernameInput.setVisible(true);
        cmnd.setVisible(true);
        status.setText("Disconnected");
      }
    };
    disconnectButton.setOnAction(event);

    Text statusText = new Text("Status: ");
    statusText.setFont(Font.font("arial", FontWeight.THIN,
                                  FontPosture.ITALIC, 12));

    StackPane inputButtonStack = new StackPane(disconnectButton, usernameInput);

    HBox hboxConnect = new HBox(statusText, status);
    VBox vboxConnect = new VBox(cmnd, inputButtonStack, hboxConnect, usersPane);
    vboxConnect.setSpacing(10);

    /***********message input*******************/

    Text messageTitle = new Text("Chat to other users");
    messageTitle.setFont(Font.font("arial", FontWeight.BOLD, FontPosture.REGULAR,
                           15));

    TextArea messageInput = new TextArea();
    messageInput.setPrefColumnCount(30);
    messageInput.setPrefRowCount(5);

    Text sendcmd = new Text("Send to");
    ComboBox<String> recipient = new ComboBox<String>();
    recipient.getItems().addAll("todo:add users");
    recipient.setVisibleRowCount(4);
    recipient.setValue("All");

    Button sendButton = new Button("Send");

    Label check = new Label("Please enter a message.");
    check.setFont(Font.font("arial", FontWeight.THIN,FontPosture.ITALIC, 12));
    check.setVisible(false);

    HBox send = new HBox(sendcmd, recipient, sendButton, check);
    send.setMargin(sendcmd, new Insets(10));
    send.setMargin(check, new Insets(10));
    send.setSpacing(5);

    //handle send button
    event = new EventHandler<ActionEvent>() {
      @Override
      public void handle(ActionEvent e) {
        String s = messageInput.getText();
        if (s.trim().isEmpty()) {
          check.setVisible(true);
        } else {
          //send message.
          String to = recipient.getValue();
          //just checks:
          check.setText(to);
          check.setVisible(true);
        }
      }
    };
    sendButton.setOnAction(event);

    /**********messages received****************/
    Text receivedTitle = new Text("Messages Received");
    receivedTitle.setFont(Font.font("arial", FontWeight.BOLD, FontPosture.REGULAR,
                           15));

    Text received = new Text("No messages to show yet");
    ScrollPane scrollPane = new ScrollPane(received);
    scrollPane.setHbarPolicy(ScrollBarPolicy.NEVER);
    scrollPane.setVbarPolicy(ScrollBarPolicy.AS_NEEDED);

    /********message area layout****************/
    VBox messageArea = new VBox(messageTitle, messageInput, send, receivedTitle,
                                scrollPane);
    messageArea.setMargin(send, new Insets(10));
    messageArea.setMargin(scrollPane, new Insets(10));
    messageArea.setMargin(messageTitle, new Insets(10));
    messageArea.setMargin(receivedTitle, new Insets(10));

    /************Assemble**********************/
    BorderPane layout = new BorderPane();
    layout.setTop(title);
    layout.setLeft(vboxConnect);
    layout.setRight(messageArea);

    layout.setMargin(messageArea, new Insets(25));
    layout.setMargin(title, new Insets(25));
    layout.setMargin(vboxConnect, new Insets(25));
    /*******************************************/

    Group root = new Group(layout);
    Scene scene = new Scene(root, 800, 600);
    scene.setFill(Color.WHITE);

    primaryStage.setTitle("The Chatroom");
    primaryStage.setScene(scene);

    primaryStage.show();
  }

  public static void main(String[] args) {
    //GuiAppl testrun = new GuiAppl();
    launch(args);
  }

}
