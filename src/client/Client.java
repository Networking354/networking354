import java.net.*;
import java.util.*;
import java.io.*;
public class Client {

  public static void main(String[] args) {
    Client client = new Client();

    String name = args[0];
    int port = Integer.parseInt(args[1]);
    client.run(name, port);

  }

  public static void run(String name, int port){
    try {
      Socket clientSocket = new Socket(name, port);
      OutputStream toServ = clientSocket.getOutputStream();
      DataOutputStream out = new DataOutputStream(toServ);
      out.writeUTF("client speaks");

      InputStream fromServ = clientSocket.getInputStream();
      DataInputStream in = new DataInputStream(fromServ);
      System.out.println("Server : " + in.readUTF());
      clientSocket.close();

    } catch(Exception e) {
      System.out.println("Exception");

      e.printStackTrace();
    }
  }
}
