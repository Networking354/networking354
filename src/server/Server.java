import java.net.*;
import java.util.*;
import java.io.*;
import java.text.*;
import java.util.concurrent.*;

public class Server extends Thread {
  /*creating the HashMap */
  Map<String, Socket> activeClients = new ConcurrentHashMap<String, Socket>();
  int numOfClients = 0;

  public static void main(String[] args) {
    Server server = new Server();
    server.run();
  }

  public void run() {

    int key = 0;

    try {
      System.out.println("1");
      /*Connecting to the client*/
      ServerSocket servSocket = new ServerSocket(8000);
      servSocket.setSoTimeout(20000);
      Socket server= servSocket.accept();

      System.out.println("2");

      DataInputStream input = new DataInputStream(server.getInputStream());
      DataOutputStream output = new DataOutputStream(server.getOutputStream());
      /*creating a new object for the client*/
      ClientHandler user = new ClientHandler(server, input , output);

      /*adding client to the ConcurrentHashmap*/
      activeClients.put(key, user);

      Thread t = new Thread(user);

      t.start();
      /*incrementing the key for the next client*/
      key++;


      output.writeUTF("this worked");
      server.close();

    } catch(Exception e) {
      System.out.println("Exception");
      e.printStackTrace();
    }

  }
}
