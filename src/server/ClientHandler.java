import java.net.*;
import java.util.*;
import java.io.*;
import java.text.*;
import java.util.concurrent.*;

public class ClientHandler extends Thread {


  final DataInputStream in;
  final DataOutputStream out;
  final Socket s;
  boolean loggedIn;

  public static void main(String []args){

  }

  public ClientHandler(Socket s, DataInputStream in , DataOutputStream out){
    this.s = s;
    this.in = in;
    this.out = out;
    this.loggedIn = true;
  }

  public void run(){

    String status;
    while (true)
    {
      try
      {
        status = out.readUTF();
        /* disconnecting upon login request*/
        if(status.equals("Logout")){
          this.loggedIn = false;
          this.s.close();
          break;
        }
      }
        catch(Exception e) {
        System.out.println("Exception");
        e.printStackTrace();
      }
    }

  }

}
