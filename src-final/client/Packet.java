package server;

public class Packet {

  public enum Control {
    CONNECTED,
    DISCONNECT,
    MESSAGE,
    USERNAME,
    USERS;
  }

  private Control ctrl;
  private String content;

  public Packet(Control ctrl, String content) {
    this.ctrl = ctrl;
    this.content = content;
  }

  public Control getControlMsg() {
    return this.ctrl;
  }

  public String getContent() {
    return this.content;
  }

}
